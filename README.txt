**Initial comments:
- I enjoyed this project and there were more things I would have liked to do on this app, but I decided to turn in what I have and explain my reasoning for some of the things that I have implemented. 

** Some Reasonings:
- I decided to write my own JSON parser. My reasoning for this is because I figured it would showcase my skills a little bit more as opposed to using the native one available in Android and Java. 
- I also decided to use a Recycler view because I found the Recycler view to be a quite efficient view to use when handling a large set of items, it also would allow for adding more items dynamically if I wanted to. 
- The animations that I created were created to get a sort of "always moving" feel to the app. I didn't like to instant snappiness of the fragment alone so I decided to create a sliding animation from the right into main view when an offer is tapped. When the user hits a back button the fragment then slides right to left out of the main view showing the list of offers. I felt that this was a clean transition that is almost unnoticable and I feel is natural feeling of the application itself. 
- I decided to make the colors of the app the way that I did because I felt that I was to imitate the style of the iBotta application itself. I didn't want to copy the theme completely, but I did want to go for a theme reminiscent of iBotta. 
- I also decided to use the Picasso library because it is an incredibly efficient way of rendering photos. I originally built my own structure for loading the pictures, but the load time would reach up to one minute and for a user that is an eternity. After using the Picasso library the load time seems almost instantaneous. 

** Future thoughts and Idea:
- Through working on this project there were a couple ideas that I had in mind to make this even better given enough time: 
	1.) Implement a search: This would let users quickly look for certain offers on certain items greatly reducing their time searching for it manually. 
	2.) Implement a favorites list: This would hold a list of items that the user has favorited. It would also allow them to quickly remove those items as well as see a total of their potential cash back.

**Update: 
- Crash bug: The reason why the app was crashing was because I initally was making ItemInfo Serializable. I briefly forgot that when made serializable everything inside the class must also be serializable and the problem was the storing of the bitmap inside this class. This made the class not serializable which caused the crash whenever a user would hit the home button after opening a fragment. I removed Serializable modifier and had to send the item position to the openItemFragment and if the user decides to favorite or unfavorite, the MainActivity has a method that will favorite the item based on the item position sent in by the openItemFragment. 
