package mobiledevtest.ibotta.devtest_roger_hannagan;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import mobiledevtest.ibotta.devtest_roger_hannagan.adapters.RecyclerViewAdapter;
import mobiledevtest.ibotta.devtest_roger_hannagan.backend.ItemInfo;
import mobiledevtest.ibotta.devtest_roger_hannagan.backend.JSONParser;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView; // holds the app recycler view.
    private Context context; // holds a copy of the current context.
    private RecyclerView.Adapter recyclerViewAdapter; // holds a copy of the recycler view
    private RecyclerView.LayoutManager recyclerViewLayoutManager; // holds a copy of the recycler view's layout manager.
    private ArrayList<ItemInfo> items; // holds all of the items retrieved from the JSON file
    private boolean fragmentOpened = false; // tells the application that the fragment is opened we need to reset the views very important.
    private Fragment currentFrag; // holds a copy of the current fragment.

    // tells the system that a fragment is opened.
    public void setFragmentOpened() {
        fragmentOpened = true;
    }

    // sets the current fragment being used. This is primarily used to remove the fragment cleanly.
    public void setCurrentFrag(Fragment currentFrag) {
        this.currentFrag = currentFrag;
    }

    // hides the recycler view from the app. This is used mainly for a fragment.
    public void hideRecyclerView() {
        recyclerView.setVisibility(View.GONE);
    }

    // used by openItemFragment to change the favorite state.
    public void favoriteItem(int pos)
    {
        items.get(pos).favorite();
    }

    // used by openItemFragment to change the favorite state.
    public void unfavoriteItem(int pos)
    {
        items.get(pos).unfavorite();
    }

    // Grabs the JSON file from resources and loads it to an actual file.
    private String readJSONFile()
    {
        String json = null; // will hold the json file.

        try {
            InputStream input = getBaseContext().getResources().openRawResource(R.raw.offers); // grab the json file itself.
            int size = input.available(); // gab the total size of the json file.
            byte[] buffer = new byte[size]; // create a buffer the size of the json file.
            input.read(buffer); // read the json file into the buffer.
            input.close();

            json = new String(buffer, "UTF-8"); // create the json string.
        }
        catch (IOException ex) {
            System.out.println("something failed while reading the json file");
            ex.printStackTrace(); // display the stack trace if error occurrs.
            return null;
        }

        return json;
    }

    // takes in a list of items and populates the UI.
    private void setupUI(ArrayList<ItemInfo> items)
    {
        // setup the recycler view.
        recyclerViewLayoutManager = new GridLayoutManager(context, 2); // set the number of items in the gridview to hold 2 per row
        recyclerViewAdapter = new RecyclerViewAdapter(context, items, this); // create the view adapter and show the items inside the view.
        recyclerView.setAdapter(recyclerViewAdapter); // set the adapter for the lists.
        recyclerView.setLayoutManager(recyclerViewLayoutManager); // set the layout manager.
        recyclerViewAdapter.notifyDataSetChanged();

    }

    // This is the first method called, generating the UI.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();
        recyclerView = (RecyclerView) findViewById(R.id.mainRecyclerView);

        if(items == null) // if the items are null then we need to parse and set them otherwise, do not redo it and just reset up the UI.
        {
            String jsonFile = readJSONFile(); // grab and read in the provided json file.
            JSONParser parser = new JSONParser(this); // takes a copy of the mainfragment activity to be able to send back a partially loaded list of items.
            parser.parseJSONFile(jsonFile); // parses the json file that was read in from the file.

            items = parser.getItems(); // extract the items from the json file.
        }

        setupUI(items); // sets up the UI for the application.

    }

    // handles the logic when the user hits the back arrow on the toolbar.
    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed(); // simply calls the back button to handle the logic of closing everything down.
        return true;
    }

    // handles the logic for when the user presses the back button.
    @Override
    public void onBackPressed() {

        if(fragmentOpened == true)
        {
            // hide the back button once pressed.
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);

            recyclerView.setVisibility(View.VISIBLE); // make the recycler view visible again.

            // shows updates the icons to show the list updating the values like we want them to.
            recyclerView.invalidate();
            recyclerViewAdapter.notifyDataSetChanged();
            fragmentOpened = false; // tell the system that a fragment is no longer open.

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.animator.slide_left_to_right, R.animator.slide_left_to_right);
            transaction.remove(currentFrag); // remove the current fragment.
            transaction.commit(); // commit the changes.
            super.onBackPressed();
        }
    }
}
