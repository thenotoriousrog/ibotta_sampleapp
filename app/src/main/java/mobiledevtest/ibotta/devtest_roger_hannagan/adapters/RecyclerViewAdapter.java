package mobiledevtest.ibotta.devtest_roger_hannagan.adapters;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import mobiledevtest.ibotta.devtest_roger_hannagan.MainActivity;
import mobiledevtest.ibotta.devtest_roger_hannagan.R;
import mobiledevtest.ibotta.devtest_roger_hannagan.backend.ItemInfo;
import mobiledevtest.ibotta.devtest_roger_hannagan.fragments.OpenItemFragment;

/**
 * Created Roger Hannagan for the iBottaDevTest on 11/30/17.
 * This is the adapter class for the MainRecyclerView.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private Context context;
    private ArrayList<ItemInfo> items; // holds the list of items that we want to display to the users.
    private static MainActivity mainActivity; // holds a copy of the mainActivity.

    // Simple constructor
    public RecyclerViewAdapter(Context context, ArrayList<ItemInfo> items, MainActivity mainActivity)
    {
        this.items = items;
        this.context = context;
        this.mainActivity = mainActivity;
    }

    // This is a custom recycler view holder for the recycler view adapter.
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView itemPrice; // holds the cash back value of the item.
        public TextView itemName; // holds the description of the item.
        public ImageView itemImage; // holds the image of the item.
        public ImageView favImage; // holds the image favorites button.
        public CardView cardItem; // holds the cardView for that item.

        public RecyclerViewHolder(View v)
        {
            super(v);
            itemPrice = (TextView) v.findViewById(R.id.itemPrice);
            itemName = (TextView) v.findViewById(R.id.itemName);
            itemImage = (ImageView) v.findViewById(R.id.itemImage);
            favImage = (ImageView) v.findViewById(R.id.favoritesImage);
            cardItem = (CardView) v.findViewById(R.id.offersCardView);

            Display display = mainActivity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x; // get the width of the device.
            double ratio = ((float)width/300.0);
            int height = (int)(ratio*100); // get the height of the device.

            cardItem.getLayoutParams().width = width; // set the new width.
            cardItem.getLayoutParams().height = height;
        }
    }

    // Generates the view and and viewHolder and returns it.
    @Override
    public RecyclerViewAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_items, parent, false); // the view created by recycler_view_items.

        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view); // container for all of the view.
        return viewHolder;
    }


    // Sets the items for the view itself. Incredibly important.
    @Override
    public void onBindViewHolder(final RecyclerViewAdapter.RecyclerViewHolder holder, final int position)
    {
        holder.itemPrice.setText(items.get(position).getItemCurrentValue()); // set the cash value for this item.
        holder.itemName.setText(items.get(position).getItemName()); // set the name for this item.

        String url = items.get(position).getImageURL(); // get the url from the item

        // picasso is a much faster way of loading and using images in application design.
        Picasso.with(this.context)
                .load(url)
                .error(R.drawable.defaultimg) // the default image if there is an error with parsing the image.
                .into(holder.itemImage); // sets the item image that we want to store this with.



        //set the click listener for the favorites item.
        holder.favImage.setImageBitmap(items.get(position).getFavBitmapState()); // set the current bitmap state.
        holder.favImage.setOnClickListener(new View.OnClickListener() {

            // favorites the item and switches the favorites image.
            @Override
            public void onClick(View view)
            {
                if(items.get(position).isFavorited() == true) { // item is favorited need to unfavorite.
                    items.get(position).unfavorite(); // unfavorites and switches the icon.
                    holder.favImage.setImageBitmap(items.get(position).getFavBitmapState()); // get the fav bitmap state.
                    System.out.println("favorited: " +  items.get(position).getFavBitmapState());
                }
                else {
                    items.get(position).favorite(); // unfavorites and switches the icon.
                    holder.favImage.setImageBitmap(items.get(position).getFavBitmapState()); // get the fav bitmap state.
                    System.out.println("unfavorited: " +  items.get(position).getFavBitmapState());
                }

                holder.favImage.invalidate();

            }
        });

        // set the click listener for the cardItem.
        holder.cardItem.setOnClickListener(new View.OnClickListener() {

            // on Click, open the OpenItemFragment.
            @Override
            public void onClick(View view)
            {
                System.out.println("testing testing testing");

                FragmentTransaction fragmentTransaction = mainActivity.getFragmentManager().beginTransaction(); // start a fragment transaction.
                fragmentTransaction.setCustomAnimations(R.animator.slide_right_to_left, R.animator.slide_left_to_right); // animations for opening and closing a fragment.
                Fragment openItemFragment  = new OpenItemFragment(); // creates a new open item fragment.
                Bundle fragmentArgs = new Bundle(); // holds the arguements for the fragment.
                fragmentArgs.putString("imageURL", items.get(position).getImageURL());
                fragmentArgs.putString("itemName", items.get(position).getItemName());
                fragmentArgs.putString("itemValue", items.get(position).getItemCurrentValue());
                fragmentArgs.putString("itemDesc", items.get(position).getItemDescription());
                fragmentArgs.putString("itemTerms", items.get(position).getItemTerms());
                fragmentArgs.putBoolean("isFavorited", items.get(position).isFavorited()); // holds the current favorited state of the item clicked.
                fragmentArgs.putInt("itemNum", position); // sets the position of the item that was clicked.

                //fragmentArgs.putSerializable("item", items.get(position)); // send in the item to the fragment manager.
                openItemFragment.setArguments(fragmentArgs); // set the arguments for the fragment.

                // begin displaying the fragment.
                fragmentTransaction.replace(R.id.mainContentScreen, openItemFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit(); // commit the fragment transaction to show the fragment.
                mainActivity.setFragmentOpened(); // tell the mainactivity that fragment is opened.
                mainActivity.setCurrentFrag(openItemFragment); // set the main fragment for the current item.
                mainActivity.hideRecyclerView(); // hide the recycler view.
            }
        });


    }

    // gets the number of items in the list sent in by the adapter.
    @Override
    public int getItemCount(){

        return items.size();
    }

}
