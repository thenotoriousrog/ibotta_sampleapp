package mobiledevtest.ibotta.devtest_roger_hannagan.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import mobiledevtest.ibotta.devtest_roger_hannagan.MainActivity;
import mobiledevtest.ibotta.devtest_roger_hannagan.R;
import mobiledevtest.ibotta.devtest_roger_hannagan.backend.BitmapWorkshop;
import mobiledevtest.ibotta.devtest_roger_hannagan.backend.ItemInfo;

/**
 * Created by Roger on 12/1/17.
 * Opens up a fragment that displays the item that the user has clicked in more detail.
 */

public class OpenItemFragment extends Fragment {

    private String imageURL; // holds the url of the image.
    private String itemName; // holds the name of the item.
    private String itemValue; // holds the cash back value of the item
    private String itemDesc; // holds the description of the item
    private String itemTerms; // holds the terms of the item.
    private ItemInfo currItem; // holds the current item.
    private boolean isFavorited = false; // holds if the item has been favorited or not.
    private int itemPos = -1;

    // Very first method called, grabs the arguements passed in and populates the values needed for onCreateView.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments(); // get the arguments that were passed in.

        // grab and set variables.
        imageURL = args.getString("imageURL");
        itemName = args.getString("itemName");
        itemValue = args.getString("itemValue");
        itemDesc =  args.getString("itemDesc");
        itemTerms = args.getString("itemTerms");
        isFavorited = args.getBoolean("isFavorited");
        itemPos = args.getInt("itemNum");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View openItemFragment = inflater.inflate(R.layout.open_item, container, false);
        final MainActivity mainActivity = (MainActivity) getActivity(); // creates a mainactivity instance.

        // set all items inside the view.
        TextView openItemName = (TextView) openItemFragment.findViewById(R.id.openItemName); // grab the item name textview
        openItemName.setText(itemName);
        TextView openItemValue = (TextView) openItemFragment.findViewById(R.id.openItemValue); // grab the item value textview.
        openItemValue.setText(itemValue);
        TextView openItemDesc = (TextView) openItemFragment.findViewById(R.id.openItemDescription); // grab the item desc textview.
        openItemDesc.setText(itemDesc);
        TextView openItemTerms = (TextView) openItemFragment.findViewById(R.id.openItemTerms); // grab the item terms textview.
        openItemTerms.setText(itemTerms);
        openItemTerms.setMovementMethod(new ScrollingMovementMethod()); // allows for scrolling in our textview.

        RelativeLayout imageBackground = (RelativeLayout) openItemFragment.findViewById(R.id.imageBackground); // the layout holding the image background.
        ImageView openItemImage = (ImageView) imageBackground.findViewById(R.id.openItemPicture); // grab the item image imageView.
        final ImageView favImage = (ImageView) openItemFragment.findViewById(R.id.favorites); // grabs the favorites imageView

        if(isFavorited == true) // set the initial image, if favorited, ensure image is highlighted.
        {
            Bitmap favImg = BitmapWorkshop.decodeSampledBitmapFromResource(getResources(), R.drawable.favorited, 100, 100);
            favImage.setImageBitmap(favImg); // switch the bitmap.
        }
        else // set the initial image, ensure image is not highlighted.
        {
            Bitmap favImg = BitmapWorkshop.decodeSampledBitmapFromResource(getResources(), R.drawable.unfavorited, 100, 100);
            favImage.setImageBitmap(favImg); // switch the bitmap.

        }

        // click listener for when a user toggles favorites on and off.
        favImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                if(isFavorited == true) // if item is currently favorited, unfavorite.
                {
                    //currItem.unfavorite(); // unfavorite the item.
                    mainActivity.unfavoriteItem(itemPos);
                    Bitmap favImg = BitmapWorkshop.decodeSampledBitmapFromResource(getResources(), R.drawable.unfavorited, 100, 100);
                    favImage.setImageBitmap(favImg); // switch the bitmap.
                }
                else // image is unfavorited, so favorite it.
                {
                    mainActivity.favoriteItem(itemPos);
                    //currItem.favorite(); // favorite the item.
                    Bitmap favImg = BitmapWorkshop.decodeSampledBitmapFromResource(getResources(), R.drawable.favorited, 100, 100);
                    favImage.setImageBitmap(favImg); // switch the bitmap.

                }
            }
        });


        // grab and load the item into the openImageView efficiently with picasso.
        Picasso.with(getContext())
                .load(imageURL)
                .error(R.drawable.defaultimg)
                .into(openItemImage);

        // show the back button.
        mainActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return openItemFragment; // returns the view to be displayed.
    }

}
