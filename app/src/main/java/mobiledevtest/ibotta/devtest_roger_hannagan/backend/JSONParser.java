package mobiledevtest.ibotta.devtest_roger_hannagan.backend;

import android.content.Context;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Roger on 11/30/17.
 * This class takes in a json file and parses it. Note: this class is built to handle the json file given by iBotta.
 */

public class JSONParser {

    private ArrayList<ItemInfo> items = new ArrayList<>(); // holds all of the items parsed from the json file.
    private int ID = -1; // holds the ID value.
    private String imageURL; // holds an item's image url.
    private String itemName; // holds the name of the item of the string.
    private String description; // holds the description of an item.
    private String terms; // holds the terms of an item.
    private String currentValue; // holds the current value of an item.
    private Context context; // holds the app context.

    public JSONParser(Context context)
    {
        this.context = context;
    }

    // removes the unwanted characters from the json file.
    private String removeChars(String json)
    {
        // remove all of the unwanted characters from the json file.
        json.replace("\"", "");
        json.replace("{", "");
        json.replace("}", "");
        json.replace("[", "");
        json.replace("]", "");

        return json; // return the updated json string.
    }

    // resets all of the items into an empty string.
    private void resetVals()
    {
        ID = -1;
        imageURL = "";
        itemName = "";
        description = "";
        terms = "";
        currentValue = "";
    }

    // takes in a string and adds and remove the newlines as needed.
    private String parseAddAndRemoveNewLines(String item)
    {

        char[] str = item.toCharArray();
        for(int i = 0; i < item.length()-1; i++)
        {
            if(item.charAt(i) == '\\' && item.charAt(i+1) == 'n' )
            {
                str[i] = '\n';
                str[i+1] = '\0'; // set to no char
            }
            else if(item.charAt(i) == '\\' && item.charAt(i+1) == 'r')
            {
                str[i] = '\0'; // set to no char
                str[i+1] = '\0'; // set to no char
            }
        }

        item = new String(str); // set the new string.
        return item;
    }

    // Takes in a string item and parses it into the correct location.
    private void parseAndPlaceItem(String category, String item)
    {
        if(category.equals("id")) {
            ID = Integer.parseInt(item); // grabs the item number and converts it to an integer.
        } else if(category.equals("url")) {
            imageURL = item;
        } else if(category.equals("name")) {
            itemName = item;
        } else if(category.equals("description")) {
            description = item;
        } else if (category.equals("terms")) {
            item = parseAddAndRemoveNewLines(item); // parses adds newlines and removes the newline characters from the string to ensure proper creation of the string.
            terms = item;
        } else if(category.equals("current_value")){
            currentValue = item;
        }

    }

    // tells the program if this is valid category or not.
    private boolean validCategory(String category)
    {
        if(category.equals("id")) {
            return true;
        } else if(category.equals("url")) {
            return true;
        } else if(category.equals("name")) {
            return true;
        } else if(category.equals("description")) {
            return true;
        } else if (category.equals("terms")) {
            return true;
        } else if(category.equals("current_value")){
            return true;
        }
        else {
            return false; // not a valid category.
        }
    }

    // Parses the json file and stores it into an object.
    public void parseJSONFile(String json)
    {

        String[] largeItems = json.split("\\,(?=\\{)|(?<=\\})\\,"); // split all strings between brackets with a comma after it.

        int itemCounter = 0; // counts the number of objects keeping track of when we should create a new object.

        String category = ""; // holds the category.

        // iterate through all large items in the json file.
        for(int i = 0; i < largeItems.length; i++)
        {
            Pattern p = Pattern.compile("\"([^\"]*)\""); // grab items between quotes.
            Matcher m = p.matcher(largeItems[i]); // grab all items that follow the regex pattern.

            while(m.find()) // look for a valid group item to be found.
            {
                if(itemCounter == 12) // signals the program to create a new item info.
                {
                    ItemInfo newItem = new ItemInfo(ID, imageURL, itemName, description, terms, currentValue, context); // creates a new ItemInfo.
                    items.add(newItem); // add the new item into the list of items.
                    itemCounter = 0; // reset the counter.
                    resetVals(); // reset the vals.
                    category = "";
                }

                String str = m.group().replace("\"", ""); // grab the string to check for the item.

                if(!category.isEmpty()) {
                    parseAndPlaceItem(category, str); // parse and place items.
                    category = "";
                }

                if(validCategory(str) == true) // a valid category was chosen.
                {
                    category = m.group().replace("\"", ""); // grabs the item category, removes the quotes.
                }

                itemCounter++; // increment the counter.
            }
        }
    }

    // gets the items parsed from the
    public ArrayList<ItemInfo> getItems()
    {
        return items;
    }

}
