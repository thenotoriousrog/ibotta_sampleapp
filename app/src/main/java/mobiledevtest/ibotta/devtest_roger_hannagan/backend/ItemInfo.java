package mobiledevtest.ibotta.devtest_roger_hannagan.backend;

import android.content.Context;
import android.graphics.Bitmap;
import java.io.Serializable;
import mobiledevtest.ibotta.devtest_roger_hannagan.R;

/**
 * Created by Roger on 11/30/17.
 * Holds the information for an item that is parsed from the JSON file.
 * Removed serializability since that was causing crashes on the app whenever a fragment was opened.
 */

public class ItemInfo {

    private final  int ID; // holds the ID value.
    private final String imageURL; // holds an item's image url.
    private final String itemName; // holds the name of the item of the string.
    private final String description; // holds the description of an item.
    private final String terms; // holds the terms of an item.
    private final String currentValue; // holds the current value of an item.
    private boolean favorited = false; // holds whether or not a user has favorited (or "starred") this item.
    private Bitmap favBitmap; // holds a copy of the favbitmap
    private Context appContext; // holds the app context here.

    // simple constructor.
    public ItemInfo(int ID, String imageURL, String itemName, String description, String terms, String currentValue, Context context)
    {
        this.ID = ID;
        this.imageURL = imageURL;
        this.itemName = itemName;
        this.description = description;
        this.terms = terms;
        this.currentValue = currentValue;
        appContext = context;
        favBitmap =  BitmapWorkshop.decodeSampledBitmapFromResource(context.getResources(), R.drawable.unfavorited, 50, 50); //sets the bitmap of the image if it is favorited or not.
    }

    // returns whether or not this item is favorited.
    public boolean isFavorited()
    {
        if(favorited) {
            return true;
        }
        else {
            return false;
        }
    }

    // sets this item to be now favorited.
    public void favorite()
    {
        favorited = true;
        favBitmap = BitmapWorkshop.decodeSampledBitmapFromResource(appContext.getResources(), R.drawable.favorited, 50, 50); // decodes an optimum sized bitmap for the layout.
    }

    public void unfavorite()
    {
        favorited = false;
        favBitmap = BitmapWorkshop.decodeSampledBitmapFromResource(appContext.getResources(), R.drawable.unfavorited, 50, 50); // decodes an optimum sized bitmap for the layout.
    }

    // returns the bitmap state for the current. item.
    public Bitmap getFavBitmapState()
    {
        return favBitmap;
    }

    public int getItemID(){
        return ID;
    }

    public String getImageURL(){
        return imageURL;
    }

    public String getItemName(){
        return itemName;
    }

    public String getItemDescription(){
        return description;
    }

    public String getItemTerms() {
        return terms;
    }

    public String getItemCurrentValue() {
        return currentValue;
    }

    // prints all of the items information to the console.
    public void print() {
        System.out.println(); // new line to keep things pretty.
        System.out.println("Printing item info...");
        System.out.println("ID = " + ID);
        System.out.println("Image URL = " + imageURL);
        System.out.println("Item name = " + itemName);
        System.out.println("Description = " + description);
        System.out.println("Terms = " + terms);
        System.out.println("Current Value = " + currentValue);
        System.out.println(); // new line to keep things pretty.
    }
}
