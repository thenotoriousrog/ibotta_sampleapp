package mobiledevtest.ibotta.devtest_roger_hannagan.backend;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by thenotoriousrog on 12/3/17.
 * An automated test that checks if the Json parser parses the json file into an arraylist of items.
 */

public class TestItemInfo {

    @Test
    public void testInstanceCreation() {
        try {

            String testURL = "www.url.com"; // super basic url.
            ItemInfo item = new ItemInfo(100, testURL, "name", "generic description", "terms", "value of money", null); // creates a basic item info.
        }
        catch(Exception ex) {
            fail("Error: failed to make an itemInfo");
        }
    }

    private ItemInfo item; // basic item info.

    @Before
    public void setup() {
        String testURL = "www.url.com"; // super basic url.
        item = new ItemInfo(100, testURL, "name", "generic description", "terms", "value of money", null); // creates a basic item info.
    }

    // tests if the initial starting item is favorited, should be false.
    @Test
    public void testIfFavorited() {
        assertFalse(item.isFavorited());
    }

    // tests if favoriting an item makes its boolean qualifier true, it should.
    @Test
    public void testFavorite(){
        item.favorite();
        assertTrue(item.isFavorited());
    }

    // tests if unfavoriting an item makes its boolean qualifier false, it should.
    @Test
    public void testUnfavorite() {
        item.unfavorite();
        assertFalse(item.isFavorited());
    }


}
